//~~~~~~~~~~~~PAYMENTS SLIDE~~~~~~~~~~~~~~~~~~~

function animate1_1(){
  $("#money")
    .velocity({translateX: 62, translateY: 35}, 1500);
}

function animate1_2(){
  secret.style.visibility="visible";
  $("#secret")
    .velocity({translateX: 0, translateY: 0}, 0)
    .velocity({opacity: 1}, { duration: 1000 }, {visibility: "visible"})
    .velocity({translateX: 2, translateY: 28}, 1500);
    //.velocity({ opacity: 0 }, {duration: 1000}, {visibility: "hidden"});
}

function animate1_3(){
  question.style.visibility="visible";
  $("#question")
    .velocity({translateX: 0, translateY: 0}, 0)
    .velocity({opacity: 1}, { duration: 1000 }, {visibility: "visible"})
    .velocity({translateX: 43, translateY: -15}, 1500)
    .velocity({ opacity: 0 }, {duration: 1000}, {visibility: "hidden"});
}

function animate1_4(){
  yes.style.visibility="visible";
  $("#yes")
    .velocity({translateX: 0, translateY: 0}, 0)
    .velocity({opacity: 1}, { duration: 1000 }, {visibility: "visible"})
    .velocity({translateX: -43, translateY: 15}, 1500)
    .velocity({ opacity: 0 }, {duration: 1000}, {visibility: "hidden"});
}


function animate1_5(){
  $("#secret")
    .velocity({translateX: -13, translateY: 28}, 1500)
    .velocity({translateX: -13, translateY: 47}, 1500)
    .velocity({ opacity: 0 }, {duration: 1000}, {visibility: "hidden"});

    animateGear1_1.beginElement();
}

function animate1_6(){
  key.style.visibility="visible";
  $("#key")
    .velocity({translateX: 0, translateY: -19}, 1500)
    .velocity({translateX: 15, translateY: -19}, 1500)

    animateGear1_1.endElement();
}


function animate1_7(){
  $("#key")
    .velocity({translateX: 85, translateY: -35}, 1500)
    .velocity({ opacity: 0 }, {duration: 1000}, {visibility: "hidden"});
}

function animate1_8(){
  $("#money")
    .velocity({translateX: 7, translateY: 60}, 1500);
}

//~~~~~~~~~~~~SIGNATURES SLIDE~~~~~~~~~~~~~~~~~~~
function animate2_1(){
  packet2_1.style.visibility="visible";

  $("#packet2_1")
    .velocity({translateX: 40, translateY: -15}, 1500)
    .velocity({translateX: 40, translateY: -50}, 1500)
    .velocity({ opacity: 0 }, {duration: 1000}, {visibility: "hidden"});

  setTimeout(rejectPacket, 4000);
  setTimeout(addPIT, 1500);
}

function rejectPacket(){
  no2_1.style.visibility="visible";
  $("#no2_1").velocity("callout.flash")
  .delay(1000)
  .velocity({ opacity: 0 }, {duration: 1000}, {visibility: "hidden"});
}

function addPIT(){
  $("#pitEntry").text("/exec/A     3        1");
}

function animate2_2(){
  packet2_2.style.visibility="visible";

  $("#packet2_2")
    .velocity({translateX: -40, translateY: -15}, 1500)
    .velocity({ opacity: 0 }, {duration: 1000}, {visibility: "hidden"});
    setTimeout(addPIT2, 1500);
}

function addPIT2(){
  $("#pitEntry").text("/exec/A     3        1,2");
}

//~~~~~~~~~~~~~~~~~~~~~~~SIGNATURE CAPTURE SLIDE
function animate3_1(){
  packet3_2.style.visibility="visible";

  $("#packet3_2")
    .velocity({translateX: -40, translateY: -15}, 1500)
    .velocity({translateX: -40, translateY: -50}, 1500)
    .velocity({ opacity: 0 }, {duration: 1000}, {visibility: "hidden"});

  setTimeout(secondPacket, 1500);
}


function secondPacket(){
  packet3_1.style.visibility="visible";

    $("#packet3_1")
      .velocity({translateX: -40, translateY: 15}, 1500);
}

function animate3_2(){
  $("#packet3_1")
    .velocity({translateX: 0, translateY: 0}, 1500)
    .velocity({translateX: 0, translateY: -35}, 1500)
    .velocity({ opacity: 0 }, {duration: 1000}, {visibility: "hidden"});
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~ INTEL SGX SLIDE~~~~~~~~~~~~~~~~
function animate4_1(){
  $("#key4_2")
    .velocity({translateX: 10, translateY: -50}, 2000);
}

function animate4_2(){
  $("#gear4_2")
    .velocity({translateX: 0, translateY: -15}, 1000)
    .velocity({translateX: 75, translateY: -15}, 1500)
    .velocity({translateX: 75, translateY: 0}, 1000);

    setTimeout(switchGears, 3500);
}

function switchGears(){
  gear4_2.style.visibility="hidden";
  gear4_3.style.visibility="visible";
}

function animate4_3(){
  animateGear4_3.beginElement();
  enclave4_1.style.visibility="visible";
}

function animate4_4(){
  $("#key4_1")
    .velocity({translateX: 0, translateY: -5}, 500)
    .velocity({translateX: 75, translateY: -5}, 2000);
}

function animate4_5(){
  secret4_1.style.visibility="visible";
  setInterval(movePackets4, 4000);
}

function movePackets4(){
  $("#secret4_1")
    .velocity({translateX: 0, translateY: 0}, 0)
    .velocity({opacity: 1}, { duration: 500 }, {visibility: "visible"})
    .velocity({translateX: 10, translateY: 0}, 1000)
    .velocity({translateX: 10, translateY: 45}, 1800)
    .velocity({ opacity: 0 }, {duration: 500}, {visibility: "hidden"});
}
